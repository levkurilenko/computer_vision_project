#!/usr/bin/python
from tkinter import *
from tkinter import ttk
from tkinter import filedialog as fd
from PIL import ImageTk
from PIL import Image
from cnn_learn import learn
from cnn_infer import infer
from cnn_infer_single import infer_single

# Import CNN Libraries
import torch
import torchvision
import torchvision.transforms as transforms
from torchvision.datasets import ImageFolder

import matplotlib.pyplot as plt
import numpy as np

from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import math

import torch.optim as optim

# import _thread


def loadImage():
    filename = fd.askopenfilename(initialdir = "../../datasets")
    image_path.set(filename)
    image1 = Image.open(filename)
    image1 = image1.resize((315,315), Image.ANTIALIAS)
    # image1.thumbnail(300,Image.ANTIALIAS)
    image2 = ImageTk.PhotoImage(image1)
    imageDisp.configure(image=image2)
    root.mainloop()
    
def setDir(dirPath):
    dirName = fd.askdirectory(initialdir = "../../datasets")
    dirPath.set(dirName)
    
def inferTestset():
    console_text['state'] = 'normal'
    console_text.insert('end',"Testing with dataset...\n")
    root.update_idletasks()
    return_message = infer(net, testset_path, image_height, image_width)
    console_text.insert('end',return_message)
    console_text['state'] = 'disabled'
    root.mainloop()    
    
def inferSingle():
    console_text['state'] = 'normal'
    console_text.insert('end',"Inferring single...\n")
    root.update_idletasks()
    pred_index, return_message = infer_single(net, image_path, image_height, image_width)
    gesture_name.set(classes[pred_index])
    gest_id['state'] = 'normal'
    gest_id.replace(1.0,'end', classes[pred_index], "center")
    console_text.insert('end',return_message)
    gest_id['state'] = 'disabled'
    console_text['state'] = 'disabled'
    root.mainloop()
    
def learnParameters():
    # try:
    console_text['state'] = 'normal'
    console_text.insert('end',"Learning...\n")
    console_text['state'] = 'disabled'
    root.update_idletasks()
    global net 
    global classes
    # _thread.start_new_thread(learn,(trainset_path, image_height, image_width, kernel_size, final_channels, conv_stages, pool_dim, epochs))
    net, classes = learn(trainset_path, image_height, image_width, 
        kernel_size, final_channels, conv_stages, pool_dim, 
        epochs, nodes1, nodes2, conv1, conv2)
    print(net)
    IT_btn.configure(state="enabled")
    IS_btn.configure(state="enabled")
    console_text['state'] = 'normal'
    console_text.insert('end','Done learning.\n')
    console_text['state'] = 'disabled'
    root.mainloop()
    # except ValueError:
    #     pass

# create root and main frame
root = Tk()
root.title("Gesture Recognition")
mainframe = ttk.Frame(root, padding="12 12 12 12")
mainframe.grid(column=0, row=0, sticky=(N,W,E,S))
mainframe.columnconfigure(0, weight=1)
mainframe.rowconfigure(0, weight=1)

# string variables that the GUI can get and set
image_height = StringVar()
image_height.set("84")
image_width = StringVar()
image_width.set("84")
kernel_size = StringVar()
kernel_size.set("5")
final_channels = StringVar()
final_channels.set("50")
conv_stages = StringVar()
conv_stages.set("3")
pool_dim = StringVar()
pool_dim.set("2")
gesture_name = StringVar()
trainset_path = StringVar()
trainset_path.set('../../datasets/Marcel_Train/')
testset_path = StringVar()
testset_path.set('../../datasets/Marcel_Test/')
image_path = StringVar()
image_path.set('../../datasets/Marcel_Test/A/A-uniform01.ppm')
epochs = StringVar()
epochs.set("4")
nodes1 = StringVar()
nodes1.set("128")
nodes2 = StringVar()
nodes2.set("64")
conv1 = StringVar()
conv1.set("6")
conv2 = StringVar()
conv2.set("16")


# Load default image
image = Image.open('../../datasets/Marcel_Test/A/A-uniform01.ppm')
image = image.resize((315,315), Image.ANTIALIAS)
image = ImageTk.PhotoImage(image)


# Left column
image_container = ttk.Frame(mainframe)
image_container.grid(column=1,row=1, sticky=N)
ttk.Label(image_container, text="Input Image").grid(column=1, row=0, sticky=(S,W))
imageDisp = ttk.Label(image_container, image=image)
imageDisp.grid(column=1, row=2, sticky=(S,W))
ttk.Button(image_container, text="Load Image", command=loadImage).grid(column=1,row=3, sticky=(N,E,W))

# sub frame to hold CNN parameter fields
param_palette = ttk.Frame(mainframe)
param_palette.grid(column=1,row=2)
ttk.Label(param_palette, text="CNN PARAMETERS").grid(column=0, row=0, columnspan=2, sticky=W)
ttk.Label(param_palette, text="Image Height: ").grid(column=0, row=1, sticky=E)
image_height_entry = ttk.Entry(param_palette, width=4, textvariable=image_height)
image_height_entry.grid(column=1, row=1, sticky=W)
ttk.Label(param_palette, text="Image Width: ").grid(column=0, row=2, sticky=E)
image_width_entry = ttk.Entry(param_palette, width=4, textvariable=image_width)
image_width_entry.grid(column=1, row=2, sticky=W)
ttk.Label(param_palette, text="Kernel Size: ").grid(column=0, row=3, sticky=E)
kernel_size_entry = ttk.Entry(param_palette, width=4, textvariable=kernel_size)
kernel_size_entry.grid(column=1, row=3, sticky=W)
ttk.Label(param_palette, text="Nodes 1: ").grid(column=0, row=4, sticky=E)
nodes1_entry = ttk.Entry(param_palette, width=4, textvariable=nodes1)
nodes1_entry.grid(column=1, row=4, sticky=W)
ttk.Label(param_palette, text="Conv 1 Filters: ").grid(column=0, row=5, sticky=E)
conv1_entry = ttk.Entry(param_palette, width=4, textvariable=conv1)
conv1_entry.grid(column=1, row=5, sticky=W)

ttk.Label(param_palette, text="Final Channels: ").grid(column=2, row=1, sticky=E)
final_channels_entry = ttk.Entry(param_palette, width=4, textvariable=final_channels)
final_channels_entry.grid(column=3, row=1, sticky=W)
ttk.Label(param_palette, text="  Pool Dimensions: ").grid(column=2, row=2, sticky=E)
pool_dim_entry = ttk.Entry(param_palette, width=4, textvariable=pool_dim)
pool_dim_entry.grid(column=3, row=2, sticky=W)
ttk.Label(param_palette, text="Epochs: ").grid(column=2, row=3, sticky=E)
epochs_entry = ttk.Entry(param_palette, width=4, textvariable=epochs)
epochs_entry.grid(column=3, row=3, sticky=W)
ttk.Label(param_palette, text="Nodes 2: ").grid(column=2, row=4, sticky=E)
nodes2_entry = ttk.Entry(param_palette, width=4, textvariable=nodes2)
nodes2_entry.grid(column=3, row=4, sticky=W)
ttk.Label(param_palette, text="Conv 2 Filters: ").grid(column=2, row=5, sticky=E)
conv2_entry = ttk.Entry(param_palette, width=4, textvariable=conv2)
conv2_entry.grid(column=3, row=5, sticky=W)


# sub frame to hold data set loading widgets
load_palette = ttk.Frame(mainframe)
load_palette.grid(column=1,row=3, columnspan=3)
load_training_btn = ttk.Button(load_palette, text="Load Train Set", command=lambda:setDir(trainset_path)) # lambda lets command take args
load_training_btn.grid(column=0, row=0, sticky=(E,W), padx=(0,8))
load_training_bar = ttk.Entry(load_palette, width=90, textvariable=trainset_path)
load_training_bar.grid(column=1, row=0, sticky=E)
load_testing_btn = ttk.Button(load_palette, text="Load Test Set", command=lambda:setDir(testset_path))
load_testing_btn.grid(column=0, row=1, sticky=(E,W), padx=(0,8))
load_testing_bar = ttk.Entry(load_palette, width=90, textvariable=testset_path)
load_testing_bar.grid(column=1, row=1, sticky=E)



# # this text field displays updates about the current state of the program
# status_bar = ttk.Entry(mainframe, textvariable=status, state="readonly")
# status_bar.grid(column=1, row=4, columnspan=4, sticky = (W,E))


# Middle column
infer_palette = ttk.Frame(mainframe)
infer_palette.grid(column=2,row=1, rowspan=2, sticky=S)
ttk.Button(infer_palette, text="Learn", command=learnParameters).grid(column=1, row=1, sticky=(E,W))
IT_btn = ttk.Button(infer_palette, text="Infer Testset", command=inferTestset, state="disabled")
IT_btn.grid(column=1, row=2, sticky=(E,W))
IS_btn = ttk.Button(infer_palette, text="Infer Single", command=inferSingle,state="disabled")
IS_btn.grid(column=1, row=3, sticky=(E,W))
ttk.Label(infer_palette, text="Identified Gesture").grid(column=1, row=4, sticky=S, pady=(1,0))
gest_id = Text(infer_palette, width=10, height=1)
gest_id.grid(column = 1, row=5)
gest_id.tag_configure("center", justify='center')
gest_id.tag_add("center", 1.0, "end")
gest_id['state'] = 'disabled'

# Right column
console_container = ttk.Frame(mainframe)
console_container.grid(column=3,row=1, rowspan=2)
ttk.Label(console_container, text="Console Output").grid(column=0, row=0, sticky=(S,W))
console_text = Text(console_container, width=40,height=37)
console_text.grid(column=0,row=1)
v = ttk.Scrollbar(console_container, orient=VERTICAL, command=console_text.yview)
v.grid(column=1,row=1, sticky=(N,S))
console_text["yscrollcommand"] = v.set
console_text.insert('end','Welcome to CNN Gesture Recognition!\n')
console_text['state'] = 'disabled'

for child in mainframe.winfo_children(): child.grid_configure(padx=5, pady=5)


image_height_entry.focus()


root.mainloop()
