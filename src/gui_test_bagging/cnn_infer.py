def infer(net, testset_path, image_height, image_width):
# def infer(net, testset_path, test_image_path, image_height, image_width, image_flag):
    print("Inferring")

    # Importing CNN Libraries
    import torch
    import torchvision
    import torchvision.transforms as transforms
    from torchvision.datasets import ImageFolder
    
    import matplotlib.pyplot as plt
    import numpy as np
    
    from torch.autograd import Variable
    import torch.nn as nn
    import torch.nn.functional as F
    import math
    
    import torch.optim as optim

    print(net)

    image_h = int(image_height.get())
    image_w = int(image_width.get())

    test_data = ImageFolder(root=testset_path.get(), transform=transforms.Compose([
        transforms.Scale(image_h, 1),
        transforms.CenterCrop([image_h,image_w]),
        transforms.ToTensor()
        ]))
    testloader = torch.utils.data.DataLoader(test_data, shuffle=False)

    class_num = len(test_data.classes)
    classes = test_data.classes
    
    dataiter = iter(testloader)
    images, labels = dataiter.next()
    
    outputs = net[0](Variable(images))

    _, predicted = torch.max(outputs.data, 1)
    
    accuracy = np.array([0, 0, 0])

    for n in range(3):
        correct = 0
        total = 0
        for data in testloader:
            images, labels = data
            outputs = net[n](Variable(images))
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum()
        accuracy[n] = int(100 * correct / total)
        print("Accuracy of network ",n," = ",accuracy[n], "%")

    # Find most accurate CNN
    accuracy_max = np.argmax(accuracy)
    print("accuracy_max = ", accuracy_max)

    print("Class num: ", class_num)
    confusion_matrix = np.zeros((class_num, class_num))
    num_confuse = np.zeros((class_num,1))
    
    class_correct = list(0. for i in range(class_num))
    class_total = list(0. for i in range(class_num))

    net_predicted = np.array([0, 0, 0])
    predicted_best = 0

    correct = 0
    total = 0

    for data in testloader:
        for n in range(3):
            images, labels = data
            outputs = net[n](Variable(images))
            _, predicted = torch.max(outputs.data,1)
            net_predicted[n] = predicted[0][0]
        
        print(net_predicted)
        bin_count = np.bincount(net_predicted)
        print("bin_count = ", bin_count)
        
        mult_infer = 0
        for i in range(bin_count.size):
            if bin_count[i] > 1:
                mult_infer = 1
                break

        if mult_infer == 1:
            if net_predicted[0] == net_predicted[1]:
                predicted_best = net_predicted[0]
            elif net_predicted[0] == net_predicted[2]:
                predicted_best = net_predicted[0]
            elif net_predicted[1] == net_predicted[2]:
                predicted_best = net_predicted[1]
        else:
            predicted_best = net_predicted[accuracy_max]
        
        print("predicted_best = ", predicted_best)
        label = labels[0]
        confusion_matrix[label, predicted_best] += 1
        num_confuse[label] += 1
        c = (int(predicted_best) == labels)
        label = labels[0]
        class_correct[label] += c[0]
        class_total[label] += 1

        total += labels.size(0)
        correct += (int(predicted_best) == labels).sum()

    print('Accuracy of the network: ', int(100 * correct / total))
    return_message = ('Accuracy of the network: ' + str(int(100 * correct / total)) + "%\n")

    # for data in testloader:
    #     images, labels = data
    #     outputs = net(Variable(images))
    #     _, predicted = torch.max(outputs.data,1)
    #     label = labels[0]
    #     confusion_matrix[label, predicted[0][0]] += 1
    #     num_confuse[label] += 1
    #     c = (predicted == labels).squeeze()
    #     label = labels[0]
    #     class_correct[label] += c[0]
    #     class_total[label] += 1

    # for data in testloader:
    #     images, labels = data
    #     outputs = net(Variable(images))
    #     _, predicted = torch.max(outputs.data, 1)
    #     c = (predicted == labels).squeeze()
    #     label = labels[0]
    #     class_correct[label] += c[0]
    #     class_total[label] += 1


    # normalize confusion matrix
    for i in range(class_num):
        for j in range(class_num):
            confusion_matrix[i,j] /= num_confuse[i]
    
    print("Confusion Matrix: \r\n",confusion_matrix)
    # return_message += str(confusion_matrix)
    
        
    for i in range(class_num):
        if class_total[i] != 0:
            print('Accuracy of %5s : %2d %%' % (classes[i], 100 * class_correct[i] / class_total[i]))
            return_message += 'Accuracy of %5s : %2d %%\n' % (classes[i], 100 * class_correct[i] / class_total[i])
        else:
            print('Accuracy of %5s : 0 %%' % (classes[i]))
            return_message += 'Accuracy of %5s : 0 %%\n' % (classes[i])


    return return_message
