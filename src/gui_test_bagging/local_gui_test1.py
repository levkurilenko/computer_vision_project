#!/usr/bin/python
from tkinter import *
from tkinter import ttk
from tkinter import filedialog as fd
from PIL import ImageTk
from PIL import Image
from cnn_learn import learn
from cnn_infer import infer

# Importing CNN Libraries
import torch
import torchvision
import torchvision.transforms as transforms
from torchvision.datasets import ImageFolder

import matplotlib.pyplot as plt
import numpy as np

from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import math

import torch.optim as optim

def loadImage():
    filename = fd.askopenfilename(initialdir = "../../datasets")
    image1 = Image.open(filename)
    image1 = image1.resize((315,315), Image.ANTIALIAS)
    # image1.thumbnail(300,Image.ANTIALIAS)
    image2 = ImageTk.PhotoImage(image1)
    imageDisp.configure(image=image2)
    root.mainloop()
    
def setDir(dirPath):
    dirName = fd.askdirectory(initialdir = "../../datasets")
    dirPath.set(dirName)
    
def identifyGesture():
    infer(net, testset_path, image_height, image_width)
    gesture_name.set("A")

def learnParameters():
    # try:
    status.set("Learning...")
    global net 
    net = learn(trainset_path, image_height, image_width, kernel_size, final_channels, conv_stages, pool_dim, epochs)
    print(net)
    status.set("Done learning.")
    ID_btn.configure(state="enabled")
    root.mainloop()
    # except ValueError:
    #     pass

# create root and main frame
root = Tk()
root.title("Gesture Recognition")
mainframe = ttk.Frame(root, padding="12 12 12 12")
mainframe.grid(column=0, row=0, sticky=(N,W,E,S))
mainframe.columnconfigure(0, weight=1)
mainframe.rowconfigure(0, weight=1)

# string variables that the GUI can get and set
image_height = StringVar()
image_height.set("84")
image_width = StringVar()
image_width.set("84")
kernel_size = StringVar()
kernel_size.set("5")
final_channels = StringVar()
final_channels.set("100")
conv_stages = StringVar()
conv_stages.set("3")
pool_dim = StringVar()
pool_dim.set("2")
gesture_name = StringVar()
status = StringVar()
status.set("Please enter CNN parameters...")
trainset_path = StringVar()
testset_path = StringVar()
epochs = StringVar()
epochs.set("2")

# Load default image
image = Image.open('../../datasets/Marcel_Test/A/A-uniform01.ppm')
image = image.resize((315,315), Image.ANTIALIAS)
image = ImageTk.PhotoImage(image)


# Left column
ttk.Label(mainframe, text="Input Image").grid(column=1, row=1, sticky=(S,W))
imageDisp = ttk.Label(mainframe, image=image)
imageDisp.grid(column=1, row=2, sticky=(S,W))
ttk.Button(mainframe, text="Load Image", command=loadImage).grid(column=1,row=3, sticky=(E,W))

# make a sub frame to hold CNN parameter fields
param_palette = ttk.Frame(mainframe)
param_palette.grid(column=1,row=4)
ttk.Label(param_palette, text="CNN PARAMETERS").grid(column=0, row=0, columnspan=2, sticky=W)
ttk.Label(param_palette, text="Image Height: ").grid(column=0, row=1, sticky=E)
image_height_entry = ttk.Entry(param_palette, width=4, textvariable=image_height)
image_height_entry.grid(column=1, row=1, sticky=W)
ttk.Label(param_palette, text="Image Width: ").grid(column=0, row=2, sticky=E)
image_width_entry = ttk.Entry(param_palette, width=4, textvariable=image_width)
image_width_entry.grid(column=1, row=2, sticky=W)
ttk.Label(param_palette, text="Kernel Size: ").grid(column=0, row=3, sticky=E)
kernel_size_entry = ttk.Entry(param_palette, width=4, textvariable=kernel_size)
kernel_size_entry.grid(column=1, row=3, sticky=W)
ttk.Label(param_palette, text="Final Channels: ").grid(column=2, row=1, sticky=E)
final_channels_entry = ttk.Entry(param_palette, width=4, textvariable=final_channels)
final_channels_entry.grid(column=3, row=1, sticky=W)
ttk.Label(param_palette, text="Conv. Stages: ").grid(column=2, row=2, sticky=E)
conv_stage_entry = ttk.Entry(param_palette, width=4, textvariable=conv_stages)
conv_stage_entry.grid(column=3, row=2, sticky=W)
ttk.Label(param_palette, text="Pool Dimensions: ").grid(column=2, row=3, sticky=E)
pool_dim_entry = ttk.Entry(param_palette, width=4, textvariable=pool_dim)
pool_dim_entry.grid(column=3, row=3, sticky=W)
ttk.Label(param_palette, text="Epochs: ").grid(column=0, row=4, sticky=E)
epochs_entry = ttk.Entry(param_palette, width=4, textvariable=epochs)
epochs_entry.grid(column=1, row=4, sticky=W)

# make a sub frame to hold data set loading widgets
load_palette = ttk.Frame(mainframe)
load_palette.grid(column=1,row=5, columnspan=3)
load_training_btn = ttk.Button(load_palette, text="Load Train Set", command=lambda:setDir(trainset_path)) # lambda lets command take args
load_training_btn.grid(column=0, row=0, sticky=(E,W), padx=(0,8))
load_training_bar = ttk.Entry(load_palette, width=64, textvariable=trainset_path)
load_training_bar.grid(column=1, row=0, sticky=E)
load_testing_btn = ttk.Button(load_palette, text="Load Test Set", command=lambda:setDir(testset_path))
load_testing_btn.grid(column=0, row=1, sticky=(E,W), padx=(0,8))
load_testing_bar = ttk.Entry(load_palette, width=64, textvariable=testset_path)
load_testing_bar.grid(column=1, row=1, sticky=E)

# this text field displays updates about the current state of the program
status_bar = ttk.Entry(mainframe, textvariable=status, state="readonly")
status_bar.grid(column=1, row=6, columnspan=4, sticky = (W,E))


# Middle column
ID_btn = ttk.Button(mainframe, text="Identify", command=identifyGesture)
ID_btn.grid(column=2, row=3, sticky=S)
ID_btn.configure(state="disabled")
ttk.Button(mainframe, text="Learn", command=learnParameters).grid(column=2, row=4)


# Right column
ttk.Label(mainframe, text="Identified Gesture").grid(column=3, row=3, sticky=(S))
ttk.Entry(mainframe, textvariable=gesture_name, width=4, state="readonly", justify="center").grid(column=3, row=4, sticky=(N))


for child in mainframe.winfo_children(): child.grid_configure(padx=5, pady=5)


image_height_entry.focus()
root.bind('<Return>', identifyGesture)


root.mainloop()
