from __future__ import print_function
import torch
import numpy as np

x = torch.Tensor(5, 3)
print("x = ", x)

x_size = x.size()
print("size =", x_size)

y = torch.Tensor(5,3)

print("y = ", y)

print(x + y)

print(torch.add(x,y))

x = torch.rand(5, 3)
print("x = ", x)

x_size = x.size()
print("size =", x_size)

y = torch.rand(5,3)

print("y = ", y)

print(x + y)

print(torch.add(x,y))


result = torch.Tensor(5, 3)

torch.add(x, y, out=result)
print("torch.add =", result)

y.add_(x)

# numpy conversion example
a = np.ones(5)
b = torch.from_numpy(a)
np.add(a, 1, out=a)
print(a)
print(b)

