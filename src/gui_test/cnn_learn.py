def learn(trainset_path,image_height, image_width, kernel_size, final_channels, conv_stages, pool_dim, epochs, nodes1, nodes2, conv1, conv2):
    "Trains CNN using pytorch with specified parameters"
    
    print("Learning Initiated")

    import torch
    import torchvision
    import torchvision.transforms as transforms
    from torchvision.datasets import ImageFolder
    
    image_h = int(image_height.get())
    image_w = int(image_width.get())
    nodes1 = int(nodes1.get())
    nodes2 = int(nodes2.get())
    conv1 = int(conv1.get())
    conv2 = int(conv2.get())

    data = ImageFolder(root=trainset_path.get(), transform=transforms.Compose([
        transforms.Scale(image_h, 1),
        transforms.CenterCrop([image_h,image_w]),
        transforms.ToTensor()
            ]))
    trainloader = torch.utils.data.DataLoader(data, shuffle=True)
    
    print(data.classes)
    classes = data.classes
    #print(trainloader.classes)
    
    import matplotlib.pyplot as plt
    import numpy as np
    
    def imshow(img):
        img = img / 2 + 0.5     # unnormalize
        npimg = img.numpy()
        plt.imshow(np.transpose(npimg, (1, 2, 0)))
    
    class_num = len(data.classes)
    print("class size =", class_num)
    
    from torch.autograd import Variable
    import torch.nn as nn
    import torch.nn.functional as F
    import math
    
    kernel_size = int(kernel_size.get())
    final_channels = int(final_channels.get())
    edge = kernel_size - 1
    conv_stages = int(conv_stages.get())
    pool_dim = int(pool_dim.get())
    
    reduced_w = 0
    reduced_h = 0
    
    for i in range(conv_stages):
        if i == 0:
            reduced_w = image_w - edge
            reduced_h = image_h - edge
            reduced_w = reduced_w/2
            reduced_h = reduced_h/2
        else:
            reduced_w = reduced_w - edge
            reduced_h = reduced_h - edge
            reduced_w = reduced_w/2
            reduced_h = reduced_h/2
    
    print("reduced_w = ", reduced_w)
    print("reduced_h = ", reduced_h)
    
    class Net(nn.Module):
        def __init__(self):
            super(Net, self).__init__()
            self.conv1 = nn.Conv2d(3, conv1, kernel_size)
            self.pool = nn.MaxPool2d(pool_dim, pool_dim)
            self.conv2 = nn.Conv2d(conv1, conv2, kernel_size)
            self.conv3 = nn.Conv2d(conv2, final_channels, kernel_size)
            #self.fc1 = nn.Linear(16 * 5 * 5, 120)
            self.fc1 = nn.Linear(int(reduced_h*reduced_w*final_channels), nodes1)
            self.fc2 = nn.Linear(nodes1, nodes2)
            self.fc3 = nn.Linear(nodes2, class_num)
    
        def forward(self, x):
            x = self.pool(F.relu(self.conv1(x)))
            x = self.pool(F.relu(self.conv2(x)))
            x = self.pool(F.relu(self.conv3(x)))
            #x = x.view(-1, 16 * 5 * 5)
            x = x.view(-1, int(reduced_h*reduced_w*final_channels))
            x = F.relu(self.fc1(x))
            x = F.relu(self.fc2(x))
            x = self.fc3(x)
            return x
    
    
    net = Net()
    
    print("Net Created!")
    
    import torch.optim as optim
    
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)
    
    for epoch in range(int(epochs.get())):  # loop over the dataset multiple times
    
        running_loss = 0.0
        for i, data in enumerate(trainloader, 0):
            # get the inputs
            inputs, labels = data
            #print("data =", data)
            #print("labels =", labels)
    
            # wrap them in Variable
            inputs, labels = Variable(inputs), Variable(labels)
    
            # zero the parameter gradients
            optimizer.zero_grad()
    
            # forward + backward + optimize
            outputs = net(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()
    
            # print statistics
            running_loss += loss.data[0]
            if i % 2000 == 1999:    # print every 2000 mini-batches
                print('[%d, %5d] loss: %.3f' %
                      (epoch + 1, i + 1, running_loss / 2000))
                running_loss = 0.0
            # if i % 100 == 99:    # print every 2000 mini-batches
            #     print('[%d, %5d] loss: %.3f' %
            #           (epoch + 1, i + 1, running_loss / 100))
            #     running_loss = 0.0
    
    
    print('Finished Training')

    return net, classes
