def infer_single(net, testimage_path, image_height, image_width):
    # def infer(net, testset_path, test_image_path, image_height, image_width, image_flag):
    print("Inferring")

    # Importing CNN Libraries
    import torch
    import torchvision
    import torchvision.transforms as transforms
    from torchvision.datasets import ImageFolder
    
    import matplotlib.pyplot as plt
    import numpy as np
    
    from torch.autograd import Variable
    import torch.nn as nn
    import torch.nn.functional as F
    import math
    
    import torch.optim as optim
    from PIL import Image

    print(net)

    image_h = int(image_height.get())
    image_w = int(image_width.get())

    loader = transforms.Compose([
        transforms.Scale(image_h, 1),
        transforms.CenterCrop([image_h,image_w]),
        transforms.ToTensor()
        ])

    def image_loader(image_name):
        image = Image.open(image_name)
        image = loader(image).float()
        image = Variable(image, requires_grad=True)
        image = image.unsqueeze(0)  #this is for VGG, may not be needed for ResNet
        return image
    
    image = image_loader(testimage_path.get())
    
    print("net(image)", net(image))
    outputs = net(image)
    _, predicted = torch.max(outputs.data, 1)

    print("predicted = ", predicted[0][0])

    pred_index = predicted[0][0]


    return_message = "predicted = " + str(predicted[0][0]) + "\n"
    
    return pred_index, return_message
